import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/auth/Login'

Vue.use(Router)


const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
            meta : {
                title : 'Login'
            },
        },
        {
            path : '/signup',
            name : 'signup',
            component: () => import('./components/auth/SignUp.vue'),
            meta : {
                title : 'Sign Up'
            },
        },
        {
            path: '/dash',
            name: 'dash',
            component: () => import('./components/dashboard/Dashboard.vue'),
            meta : {
                title : 'Home',
                requiresAuth: true
            },
        },
        {
            path: '/home',
            name : 'home',
            component: () => import('./components/Home.vue'),
            meta : {
                title : 'Home',
                requiresAuth: true
            }

        },
    ]
})

router.afterEach((to, from, next) => {
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta);
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

});

router.beforeEach((to, from, next) => {

    const requiresAuth = to.matched.some(record  => record.meta.requiresAuth);
    const currentSession = Vue.localStorage.get('session');

    if (requiresAuth && currentSession == null) {
        console.log("a");
        //store.dispatch('showSnackbar',{ text: "Unauthenticated!. Please logged in" });
        next('/')

    } else if (requiresAuth && currentSession != null) {
        console.log("b");
        setAuthHeaders();
        next()

    } else if (currentSession){
        setAuthHeaders()
        next('/dash')
        console.log(next())
    }
    else {
        console.log("c");
        next()

    }
});

var setAuthHeaders = function () {
    if (Vue.localStorage.get('session')) {
        var session = JSON.parse(Vue.localStorage.get('session'));
        if (session.client_id != null && session.access_token != null) {
            var rawStr = session.client_id + ':' + session.secret_token;
            var wordArray = cryptojs.enc.Utf8.parse(rawStr);
            var base64 = cryptojs.enc.Base64.stringify(wordArray);
            axios.defaults.headers.common['Authorization'] = 'Basic ' + base64;
        }
    }

};

export default router
