import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        success_snackbar: {
            visible: false,
            text: null,
            timeout: 6000,
            multiline: false
        },
        error_snackbar: {
            visible: false,
            text: null,
            timeout: 6000,
            multiline: false
        },
    },
    mutations: {
        showSuccessSnackbar(state, payload) {
            state.success_snackbar.text = payload.text
            state.success_snackbar.multiline = (payload.text.length > 50) ? true : false

            if (payload.multiline) {
                state.success_snackbar.multiline = payload.multiline
            }

            if (payload.timeout) {
                state.success_snackbar.timeout = payload.timeout
            }

            state.success_snackbar.visible = true
        },
        closeSuccessSnackbar(state) {
            state.success_snackbar.visible = false;
            state.success_snackbar.multiline = false;
            state.success_snackbar.timeout = 6000;
            state.success_snackbar.text = null
        },
        showErrorSnackbar(state, payload) {
            state.error_snackbar.text = payload.text
            state.error_snackbar.multiline = (payload.text.length > 50) ? true : false

            if (payload.multiline) {
                state.error_snackbar.multiline = payload.multiline
            }

            if (payload.timeout) {
                state.error_snackbar.timeout = payload.timeout
            }

            state.error_snackbar.visible = true
        },
        closeErrorSnackbar(state) {
            state.error_snackbar.visible = false;
            state.error_snackbar.multiline = false;
            state.error_snackbar.timeout = 6000;
            state.error_snackbar.text = null
        },

    },
    actions: {
        closeSuccessSnackbar({commit}){
            commit('closeSuccessSnackbar')
        },
        showSuccessSnackbar({commit}, payload){
            commit('showSuccessSnackbar', payload)
        },
        closeErrorSnackbar({commit}){
            commit('closeErrorSnackbar')
        },
        showErrorSnackbar({commit}, payload){
            commit('showErrorSnackbar', payload)
        },
    }
})
