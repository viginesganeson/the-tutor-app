import axios from 'axios';
import store from '../store'
import router from '@/router'

export default {
    methods: {
        postUser (params) {
            return axios
                .post('/auth/signup', params)
                .then(response =>{
                    store.dispatch('showSuccessSnackbar',{ text: 'Signup success' });
                    router.push('/');
                    return response.data
                })
                .catch(error =>{
                    store.dispatch('showErrorSnackbar',{ text: error.response.data.message });
                })
        },
    }
}
