import axios from 'axios';
import store from '../store'
import router from '@/router'

export default {
    methods: {
        postSession (params) {
            return axios
                .post('/auth/login', params)
                .then(response =>{
                    store.dispatch('showSuccessSnackbar',{ text: 'Login success' });
                    router.push('/dash');
                    return response.data
                })
                .catch(error =>{
                    store.dispatch('showErrorSnackbar',{ text: error.response.data.message });
                })
        },
    }
}
