import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import {sync} from  'vuex-router-sync'
import VueLocalStorage from 'vue-localstorage'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
    theme: {
        primary: '#039BE5', // #E53935
        secondary: colors.red.lighten4, // #FFCDD2
        accent: colors.indigo.base // #3F51B5
    }
});
Vue.use(VueLocalStorage);
Vue.use(Vuetify);
Vue.use(VueAxios,axios);
sync(store,router);

Vue.config.productionTip = true;

console.log(process.env);


const API_DOMAIN_V1 = 'http://localhost:8000/api/v1';
const HEADER_API = 'The-Tutor-Api-Key';
const API_KEY = '123456';

axios.defaults.baseURL = "https://the-tutor-api.herokuapp.com/api/v1"
axios.defaults.headers.common[HEADER_API] = API_KEY;

if (process.env === "production"){


}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
